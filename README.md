# MultiInput DNN training



## Getting started

Keras architecture capable of accepting multiple inputs, including numerical, categorical, and image data. 

For training purposes.

***

## Authors and acknowledgment
Michal Piekarski 

Based on [Keras: Multiple Inputs and Mixed Data](https://pyimagesearch.com/2019/02/04/keras-multiple-inputs-and-mixed-data/)

Dataset: [House prices image dataset](https://github.com/emanhamed/Houses-dataset)